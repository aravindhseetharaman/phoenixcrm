package PhoneixTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Browser;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class Page  {

    public static final String USERNAME = (System.getenv("BROWSERSTACK_USERNAME") != null) ? System.getenv("BROWSERSTACK_USERNAME") : "aravindhseethara_EaKQ72";
    public static final String AUTOMATE_KEY = (System.getenv("BROWSERSTACK_ACCESS_KEY") != null) ? System.getenv("BROWSERSTACK_ACCESS_KEY") : "qM2Wsb7SMinpqNMc5VAu";
    // declare remote URL in a variable
    public static final String HUB_URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub.browserstack.com/wd/hub";

        public static  WebDriver wd;
        public static PageRunner pr;
    @BeforeTest
public void startTest() throws MalformedURLException {
    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setCapability("browser","chrome");
    caps.setCapability("browser_version","106.0");
    caps.setCapability("os", "OS X");
    caps.setCapability("os_version", "Big Sur");
    caps.setCapability("name","Sample Test222");
    caps.setCapability("build"," BS Number22");
    //MutableCapabilities capabilities = new MutableCapabilities();

    //System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//")
    // intialize Selenium WebDriver
     wd =new RemoteWebDriver(new URL(HUB_URL),caps);
     pr = new PageRunner(wd);
}
       public Page() {
    }

    public Page(Browser b) {

    }

    public Page(WebDriver wb) {


    }


    @Test
    public void checkSignin() throws InterruptedException {
        String myVal = pr.loadPage().doAction("iamfd","password").signInasLocator();
        Assert.assertEquals(myVal,"iamfd");
    }

    @Test
    public void CreateJObTest() throws InterruptedException {
        pr.loadPage().doAction("iamfd","password").clickcreateJob().enterFirstName();
        System.out.println("Welci");
    }
}
