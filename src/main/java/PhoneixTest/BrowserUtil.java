package PhoneixTest;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.Browser;
import org.openqa.selenium.support.ui.Select;

public class BrowserUtil {


    private static WebDriver wd;

    public static WebDriver getWd() {
        return wd;
    }
    public void setWd(WebDriver wd) {
        BrowserUtil.wd = wd;
    }


    public BrowserUtil() {
    }


    public BrowserUtil(WebDriver wd) {
        super();
        this.wd = wd;
    }
    public BrowserUtil(Browser b) {
        System.out.println("loaded page...");
        verifyBrowser(b);
    }



    private void verifyBrowser(Browser b){
        if (b==Browser.CHROME){
            WebDriverManager.chromedriver().setup();
            wd = new ChromeDriver();
        }
        else if(b == Browser.FIREFOX){
            WebDriverManager.firefoxdriver().setup();
            wd=new FirefoxDriver();
        }



    }

    public void enterKeys(By Wbelement ,String creds ){
        WebElement webElementUserNameLocator = wd.findElement(Wbelement);
        webElementUserNameLocator.clear();
        webElementUserNameLocator.sendKeys(creds);
    }

    public void clickable(By Wbelement){
        WebElement webElementUserNameLocator = wd.findElement(Wbelement);
        webElementUserNameLocator.click();
    }

    public String readText(By Wbelement){
        WebElement webElementUserNameLocator = wd.findElement(Wbelement);
       String myVal =  webElementUserNameLocator.getText();
       return myVal;
    }

    public void SelectElements(By WebElementSelectLocator,String item){
        Select webElementSelectors = new Select(wd.findElement(WebElementSelectLocator));
        webElementSelectors.selectByValue(item);
    }

    public void getURL(String myURl){

        wd.get(myURl);
    }

}
