package PhoneixTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Browser;

public class PageRunner extends BrowserUtil{


    public PageRunner() {

    }

    public PageRunner(Browser b) {
        super(b);
    }

    public PageRunner(WebDriver wd) {
        super(wd);
    }


    public LoginPage loadPage() throws InterruptedException {
        getURL("http://phoenix.testautomationacademy.in/");
        LoginPage loginPage = new LoginPage();
        return loginPage;
    }


}
