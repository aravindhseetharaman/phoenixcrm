package PhoneixTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Browser;

public class LoginPage extends BrowserUtil{

    private static final By USERNAMELOCATOR= By.xpath("//input[@id='username']");
    private static final By PASSWORDLOCATOR= By.xpath("//input[@id='password']");
    private static final By SIGNINBUTTLOCATOR = By.xpath("//span[contains(text(),' Sign in ')]");

    private static WebDriver wd;



    public LoginPage() {
    }



    public LoginPage(Browser b) {
        super(b);
    }

    public LoginPage(WebDriver wd) {
        super(wd);
    }

    public HomePagePhx doAction(String userName, String passWord) throws InterruptedException {
    Thread.sleep(4000);
    enterKeys(USERNAMELOCATOR,userName);
    enterKeys(PASSWORDLOCATOR,passWord);
    clickable(SIGNINBUTTLOCATOR);
    HomePagePhx hp = new HomePagePhx(getWd());
    return hp;
    }



}
